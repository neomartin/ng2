import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UserService } from 'src/app/services/user/user.service';
import { Subscription } from 'rxjs';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [],
})
export class HeaderComponent implements OnInit {
  public localUser: any;
  public subscraib: Subscription;
  constructor(
    public _auth: AuthService,
    private _user: UserService,
    private _router: Router
  ) {
    // console.log('thisLocal Header USer', this.localUser);

   }

  ngOnInit() {

    this._user.user$.subscribe((resp: any) => {
      // console.log('Header', resp);
      console.log('Entra', resp);
      this.localUser = resp;
      // console.log('localUSer', this.localUser);
    });
  }


}
