import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {

  transform(value: string, letters: number, reverse = false): string {
    // return value.slice(0, letters[0] + letters );
    if (reverse) {
      return value.slice(value.length - letters);
    }
    return  value.slice(0, letters);
  }

}
