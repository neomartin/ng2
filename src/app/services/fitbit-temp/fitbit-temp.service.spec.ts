import { TestBed } from '@angular/core/testing';

import { FitbitTempService } from './fitbit-temp.service';

describe('FitbitTempService', () => {
  let service: FitbitTempService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FitbitTempService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
