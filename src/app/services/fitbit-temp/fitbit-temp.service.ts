import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class FitbitTempService {
  private authorization = 'Basic MjJDNjNIOmM5Y2VmNDhiNGM4ZTcxODYwMjFkNmFiZGJmODQxZmZl';
  // private headers = new HttpHeaders();
  private URL = "https://api.fitbit.com"
  private body = new URLSearchParams();
  // Must be a Model
  private fitBitData;
  constructor(
    private _http: HttpClient,
  ) { }

  getToken(code) {
    // Set headers for token request
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    // console.log('fitbit service', code);
    this.body.set('code', code);
    this.body.set('grant_type', 'authorization_code');
    this.body.set('redirect_uri', 'https://netgames2.herokuapp.com/fitbit');
    //1 month expires token
    this.body.set('expires_in', '2592000' );  
    console.log(this.body.toString());
    headers = headers.set('Authorization', this.authorization);
    //API - Token request to the 
    return this._http.post('https://api.fitbit.com/oauth2/token', this.body.toString(), { headers: headers})
  }
  //Not working Yet
  tokenRefresh() {}

  //Get User Profile
  getUserProfile() {
    this.fitBitData = JSON.parse(localStorage.getItem("fitbitToken"));
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + this.fitBitData.access_token);
    return this._http.get(this.URL + '/1/user/-/profile.json', { headers: headers })
  }

  getUserDevices() {
    this.fitBitData = JSON.parse(localStorage.getItem("fitbitToken"));
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + this.fitBitData.access_token);
    return this._http.get(this.URL + '/1/user/-/devices.json', { headers: headers })
  }

}
