import { Component, OnInit } from '@angular/core';
import { FitbitTempService } from '../../../services/fitbit-temp/fitbit-temp.service';

@Component({
  selector: 'app-fitbit-temp',
  templateUrl: './fitbit-temp.component.html',
  styleUrls: ['./fitbit-temp.component.css']
})
export class FitbitTempComponent implements OnInit {
  private url = "";
  public code = null;
  constructor(
    private _fitbit: FitbitTempService
  ) { }

  ngOnInit(): void {
    this.url = window.location.href.split('#')[0];
    // Get url code
    this.code = this.url.split('code=')[1];
    console.log('Codigo obtenido en URL');
    console.log(this.code);
  }

  // Get URL code from fitbit OAuth
  clipboard(item) {
    let listener = (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', item);
      e.preventDefault();
    };
    document.addEventListener('copy', listener)
    document.execCommand('copy');
    navigator.clipboard.readText().then(s => {
      // code = s;
      console.log('Listened clipboard', s)
      this.getClipboardCode();  
    });
    document.removeEventListener('copy', null);
  }

  getClipboardCode() {
    let code = null;
    navigator.clipboard.readText().then(code => {
      console.log('Codigo call A', code);
      // this.copiedCode = code;
      this._fitbit.getToken(code).subscribe(resp => {
        console.log(resp);
        localStorage.setItem("fitbitToken", JSON.stringify(resp));
      });
    });
  }

  getUserData() {
    this._fitbit.getUserProfile().subscribe( (resp: any) => {
      console.log(resp.user);
    });
  }
  getUserDevices() {
    this._fitbit.getUserDevices().subscribe( resp => {
      console.log('Devices', resp);
    });
  }

}
